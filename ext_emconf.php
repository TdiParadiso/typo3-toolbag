<?php

$EM_CONF[$_EXTKEY] = [
	'title'            => 'TYPO3 Toolbag',
	'description'      => 'TYPO3 extension with useful stuff for developers',
	'category'         => 'distribution',
	'author'           => 'Paweł Schwałkowski',
	'author_email'     => 'p.schwalkowski@gmail.com',
	'state'            => 'alpha',
	'internal'         => '',
	'uploadfolder'     => '0',
	'createDirs'       => '',
	'clearCacheOnLoad' => 0,
	'version'          => '0.0.1',
	'constraints'      => [
		'depends'   => [
			'typo3' => '9.5.0-9.5.99',
		],
		'conflicts' => [],
		'suggests'  => [],
	],
];
